import filter from "lodash/filter";
import includes from "lodash/includes";

const getters = {
  getTagIcon(state) {
    return (tag) => {
      let icon = state.config.tag_options[tag] ?? "sell";
      return icon;
    };
  },
  getStats(state) {
    return state.stats;
  },
  getSubmissions(state) {
    return state.submissions;
  },
  getHighlights(state) {
    return state.highlights;
  },
  getUser(state) {
    return state.user;
  },
  getAllSubmissions(state) {
    return state.allsubmissions;
  },
  getUsers(state) {
    return state.users;
  },
  isUserAuth(state) {
    return !!state.user;
  },
  getError(state) {
    return state.error;
  },
  getConfig(state) {
    return state.config;
  },
  getIsLinkedinUser(state) {
    return state.isLinkedinUser;
  },
  canSubmit(state) {
    if (state.config == null) return false;
    if (state.submissions == 0) return true;
    if (state.config.current_phase === "judging") return false;

    // let sub = state.getSubmission(state.config.current_phase);
    let can = filter(state.submissions, (s) => {
      return (
        s.phase == state.config.current_phase &&
        includes(
          [
            "edited",
            "moderated",
            "finalised",
            "transcoded",
            "readyformoderation",
            "draft",
            "submitted",
          ],
          s.status
        )
      );
    });

    if (can.length) return false;

    //moderated, edited or finalised - means they cannt re-submit

    //if none of the above,

    //if there is one with a reject, or error, then they can resubmit
    can = filter(state.submissions, (s) => {
      return (
        s.phase == state.config.current_phase &&
        includes(["rejected", "error"], s.status)
      );
    });

    // console.log(can);

    if (can.length) return true;

    can = filter(state.submissions, (s) => {
      return s.phase == state.config.current_phase;
    });

    // console.log()
    if (can.length == 0) return true;

    return false;
  },
};

export default getters;
